﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace StackPloyDomain.UpCloud
    {
    public interface IAuthenticationHeaderProvider
        {
        public AuthenticationHeaderValue GetAuthenticationHeader();
        }
    }
