﻿using System;

using Newtonsoft.Json;
namespace StackPloyDomain.UpCloud
    {
    public class UpCloudServer
        {
        [JsonProperty("server")]
        public Server Server { get; set; }
        }

    public partial class Server
        {
        [JsonProperty("boot_order")]
        public string BootOrder { get; set; }

        [JsonProperty("core_number")]
        public long CoreNumber { get; set; }

        [JsonProperty("created")]
        public long Created { get; set; }

        [JsonProperty("firewall")]
        public string Firewall { get; set; }

        [JsonProperty("hostname")]
        public string Hostname { get; set; }

        [JsonProperty("ip_addresses")]
        public IpAddresses IpAddresses { get; set; }

        [JsonProperty("license")]
        public long License { get; set; }

        [JsonProperty("memory_amount")]
        public long MemoryAmount { get; set; }

        [JsonProperty("metadata")]
        public string Metadata { get; set; }

        [JsonProperty("networking")]
        public Networking Networking { get; set; }

        [JsonProperty("nic_model")]
        public string NicModel { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("plan")]
        public string Plan { get; set; }

        [JsonProperty("plan_ipv4_bytes")]
        public long PlanIpv4Bytes { get; set; }

        [JsonProperty("plan_ipv6_bytes")]
        public long PlanIpv6Bytes { get; set; }

        [JsonProperty("progress")]
        public long Progress { get; set; }

        [JsonProperty("remote_access_enabled")]
        public string RemoteAccessEnabled { get; set; }

        [JsonProperty("remote_access_password")]
        public string RemoteAccessPassword { get; set; }

        [JsonProperty("remote_access_type")]
        public string RemoteAccessType { get; set; }

        [JsonProperty("simple_backup")]
        public string SimpleBackup { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("storage_devices")]
        public StorageDevices StorageDevices { get; set; }

        [JsonProperty("tags")]
        public Tags Tags { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("uuid")]
        public Guid Uuid { get; set; }

        [JsonProperty("video_model")]
        public string VideoModel { get; set; }

        [JsonProperty("zone")]
        public string Zone { get; set; }
        }

    public partial class IpAddresses
        {
        [JsonProperty("ip_address")]
        public IpAddress[] IpAddress { get; set; }
        }

    public partial class IpAddress
        {
        [JsonProperty("access")]
        public string Access { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("family")]
        public string Family { get; set; }

        [JsonProperty("part_of_plan", NullValueHandling = NullValueHandling.Ignore)]
        public string PartOfPlan { get; set; }
        }

    public partial class Networking
        {
        }

    public partial class StorageDevices
        {
        [JsonProperty("storage_device")]
        public StorageDevice[] StorageDevice { get; set; }
        }

    public partial class StorageDevice
        {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("boot_disk")]
        public long BootDisk { get; set; }

        [JsonProperty("part_of_plan")]
        public string PartOfPlan { get; set; }

        [JsonProperty("storage")]
        public Guid Storage { get; set; }

        [JsonProperty("storage_size")]
        public long StorageSize { get; set; }

        [JsonProperty("storage_title")]
        public string StorageTitle { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
        }

    public partial class Tags
        {
        [JsonProperty("tag")]
        public object[] Tag { get; set; }
        }
    }
