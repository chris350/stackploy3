﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackPloyDomain.UpCloud;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StackPloyDomain
    {
    public class UpCloudService : IUpCloudService
        {
        public HttpClient Client { get;  }

        private readonly IOptions<UpCloudOptions> options;

        public UpCloudService(HttpClient client, IOptions<UpCloudOptions> options, IAuthenticationHeaderProvider authenticationHeaderProvider)
            {
            client.BaseAddress = new Uri($"{options.Value.BaseUrl}/");
            // UpCloud requires a user-agent
            client.DefaultRequestHeaders.Authorization = authenticationHeaderProvider.GetAuthenticationHeader();

            Client = client;
            this.options = options;
            }
        public async Task<UpCloudAccount> GetAccountAsync()
            {
            var response = await Client.GetAsync($"{options.Value.Version}/account");
            ///Commented out during development
            // return JsonConvert.DeserializeObject<UpCloudAccount>(await response.Content.ReadAsStringAsync());
            return new UpCloudAccount
                {
                Account = new Account
                    {
                    ResourceLimits = new ResourceLimits
                        {
                        Cores = 20,
                        DetachedFloatingIps = 30,
                        Memory = 5000,
                        Networks = 100,
                        PublicIpv4 = "50",
                        PublicIpv6 = 100,
                        StorageHdd = 10240,
                        StorageSsd = 10240
                        },
                    Username = "Upcloud User"
                    },
                };
            }
        }
    }
