﻿using Newtonsoft.Json;

namespace StackPloyDomain.UpCloud
    {
    public class UpCloudAccount
        {
        [JsonProperty("account")]
        public Account Account { get; set; }

        }

    public partial class Account
        {
        [JsonProperty("resource_limits")]
        public ResourceLimits ResourceLimits { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
        }

    public partial class ResourceLimits
        {
        [JsonProperty("cores")]
        public long Cores { get; set; }

        // ToDo identify detached floating ips structure
        [JsonProperty("detached_floating_ips")]
        public object DetachedFloatingIps { get; set; }

        [JsonProperty("memory")]
        public long Memory { get; set; }

        [JsonProperty("networks")]
        public long Networks { get; set; }

        [JsonProperty("public_ipv4")]
        public string PublicIpv4 { get; set; }

        [JsonProperty("public_ipv6")]
        public long PublicIpv6 { get; set; }

        [JsonProperty("storage_hdd")]
        public long StorageHdd { get; set; }

        [JsonProperty("storage_ssd")]
        public long StorageSsd { get; set; }
        }
    }
