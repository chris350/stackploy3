﻿using Microsoft.Extensions.Options;
using System;
using System.Net.Http.Headers;
using System.Text;

namespace StackPloyDomain.UpCloud
    {
    public class UpcloudAuthHeaderProvider : IAuthenticationHeaderProvider
        {
        private IOptions<UpCloudSecrets> secrets;

        public UpcloudAuthHeaderProvider(IOptions<UpCloudSecrets> secrets)
            {
            this.secrets = secrets;
            }
        public AuthenticationHeaderValue GetAuthenticationHeader()
            {
            return new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{secrets.Value.UserName}:{secrets.Value.Password}")));
            }
        }
    }
