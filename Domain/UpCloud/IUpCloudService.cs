﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StackPloyDomain.UpCloud
    {
    public interface IUpCloudService
        {
        Task<UpCloudAccount> GetAccountAsync();
        }
    }
