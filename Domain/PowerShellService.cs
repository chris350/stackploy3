﻿using System;
using System.Management.Automation;
using System.Threading;
using System.Threading.Tasks;

namespace StackPloyDomain
{
    public class PowerShellService
    {

        public async Task RunAzureSserver2016SetupAsync()
        {
            var script = @"New-Item D:\temp\test\test.txt";
            await this.InvokeScript(script);
        }

        public async Task<PSDataCollection<PSObject>> InvokeScript(string script)
        {
            using (PowerShell ps = PowerShell.Create())
            {
                ps.AddScript(script);
                PSDataCollection<PSObject> outputCollection = new PSDataCollection<PSObject>();

                outputCollection.DataAdded += OutputCollection_DataAdded;
                ps.Streams.Error.DataAdded += Error_DataAdded;

                IAsyncResult result = await Task.Run(() => ps.BeginInvoke<PSObject, PSObject>(null, outputCollection));
                while (result.IsCompleted == false)
                {
                    Console.WriteLine("Executing");
                    Thread.Sleep(1000);
                }
                foreach (var item in ps.Streams.Error)
                {
                    Console.WriteLine(item.ToString());
                }
                Console.WriteLine("Execution has stopped. Execution state: " + ps.InvocationStateInfo.State);
                return outputCollection;
            }
        }

        private void OutputCollection_DataAdded(object sender, DataAddedEventArgs e)
        {
            Console.WriteLine("object added to output");
        }

        private void Error_DataAdded(object sender, DataAddedEventArgs e)
        {
            Console.WriteLine("An Error was added to the event stream!");
        }
    }
}
