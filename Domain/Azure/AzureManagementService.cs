﻿using Microsoft.Azure.Management.Compute.Fluent;
using Microsoft.Azure.Management.Compute.Fluent.Models;
using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;
using Microsoft.Extensions.Logging;
using System;

namespace StackPloyDomain
{
    public class AzureManagementService : IAzureManagementService
    {
        IAzure azure;
        private readonly ILogger<AzureManagementService> _logger;

        public AzureManagementService(ILogger<AzureManagementService> logger)
        {
            this._logger = logger;
            this.azure = AzueManagementClientFactory.CreateAzureClient();
        }

        public IVirtualMachine SetupStandardWindows2016Server()
        {
            string customerName = "ClientA";
            var groupName = "stackPloyResourceGroup";

            var location = Region.USNorthCentral;

            _logger.LogInformation("Creating resource group for ..." + customerName);
            Microsoft.Azure.Management.ResourceManager.Fluent.IResourceGroup resourceGroup = null;
            try
            {
               if (!azure.ResourceGroups.Contain(groupName))
                {
                    resourceGroup = azure.ResourceGroups.Define(groupName).WithRegion(location).Create();
                }
                else
                {
                    resourceGroup = azure.ResourceGroups.GetByName(groupName);
                }
       
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Problem creating resource group");
            }
    
            if(resourceGroup != null)
            {
                _logger.LogInformation("ProvisionStatus: " + resourceGroup.ProvisioningState);
                if (resourceGroup.ProvisioningState == "Succeeded")
                {
                    //Todo fix

                    //The requested size for resource '/subscriptions/92ce1dbd-85a2-4071-a55f-feac92b6fdb9/resourceGroups/stackPloyResourceGroup/providers/Microsoft.Compute/virtualMachines/MyVirtualMachine'
                    //    is currently not available in location 'northcentralus' zones '' for subscription '92ce1dbd-85a2-4071-a55f-feac92b6fdb9'.
                    //        Please try another size or deploy to a different location or zones. See https://aka.ms/azureskunotavailable for details.
                    try
                    {
                        IVirtualMachine windowsVM = azure.VirtualMachines.Define("MyVirtualMachine")
           .WithRegion(Region.USCentral)
           .WithExistingResourceGroup(resourceGroup)
           .WithNewPrimaryNetwork("10.0.0.0/28")
           .WithPrimaryPrivateIPAddressDynamic()
           .WithNewPrimaryPublicIPAddress("MyIPAddressLabel")
           .WithLatestWindowsImage("MicrosoftWindowsServer", "WindowsServer", "2012-Datacenter")
           .WithAdminUsername("mr.doty.c@outlook.com")
           .WithAdminPassword("ow!Q0*b5&QA8")
           .WithSize(VirtualMachineSizeTypes.BasicA2)
           .Create();
                        return windowsVM;
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Problem creating VM");
                    }

                }
            }
            return null;
        }
    }
}
