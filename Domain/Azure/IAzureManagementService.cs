﻿
using Microsoft.Azure.Management.Compute.Fluent;

namespace StackPloyDomain
{
    public interface IAzureManagementService
    {

        IVirtualMachine SetupStandardWindows2016Server();
    }
}
