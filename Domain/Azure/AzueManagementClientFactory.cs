﻿using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;
using System;

namespace StackPloyDomain
{
    public  class AzueManagementClientFactory
    {
        public static IAzure CreateAzureClient()
        {
           // Environment.SetEnvironmentVariable("AZURE_AUTH_LOCATION", @"C:\Users\mrdot\source\repos\StackPloy3\StackPloy3\azureauth.properties");
            Environment.SetEnvironmentVariable("AZURE_AUTH_LOCATION", @"C:\Users\mrdot\source\repos\StackPloy3\StackPloy3\azureauth.json");
            var credentials = SdkContext.AzureCredentialsFactory
                //.FromServicePrincipal("67a97dfd-c8c5-446f-9162-4b679f11a032", "=IX9NQr]4tphrKQxY9-c-C9L]ezOcCAH", "f4a7f08e-ec39-4281-9921-4ac7cdfbdd6c", AzureEnvironment.AzureGlobalCloud);
            .FromFile(Environment.GetEnvironmentVariable("AZURE_AUTH_LOCATION"));

            var azure = Azure
                .Configure()
                .WithLogLevel(HttpLoggingDelegatingHandler.Level.Basic)
                .Authenticate(credentials)
                .WithDefaultSubscription();
            return azure;

        }
    }
}
