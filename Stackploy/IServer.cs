﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stackploy4
    {
    interface IServer
        {
        
        BootOrderType BootOrderType { get; set; }

        bool FirewallEnabled { get; set; }

        int CoreNumber { get; set; }

        DateTime CreatedDate { get; set; }



        }
    }
