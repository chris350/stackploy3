﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StackPloyDomain;
using StackPloyDomain.UpCloud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stackploy4.Controllers
    {

    [ApiController]
    [Route("[controller]")]
    public class UpCloudController
        {
        private readonly IUpCloudService upCloudService;

        public UpCloudController(IUpCloudService upCloudService)
            {
            this.upCloudService = upCloudService;
            }

        [HttpGet("Account")]
        public async Task<UpCloudAccount> Account()
            {
            return await this.upCloudService.GetAccountAsync();
            }
        }
    }
